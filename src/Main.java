import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

	
	public static Boolean isUpperCase(String s) {
		
	
        return   s.chars().allMatch((i)->Character.isUpperCase(i) ) ;
	}
	
	public static Boolean PASSWORD(String s) {
		
		
        return   s.chars().anyMatch((i)->Character.isUpperCase(i))&&  
        		s.chars().anyMatch((i)->Character.isLowerCase(i));
	}
	
	
	public static String normalize(String s) {
		
		
        return   s.toUpperCase().trim().replace(',',' ');
        		
	}
	
     public static Boolean charatevenIndex(char ch,String s) {
		
//    	 Boolean isEven=false;
//		for( int i=0;i<s.length()-1;i++) {
//			if(s.charAt(i)==ch && i%2==0) {
//				isEven=true;
//				break;
//			}
//			
//		}
//		return isEven;
    	
	return	IntStream.range(0, s.length()-1).anyMatch(i->(s.charAt(i)==ch && i%2==0));
	}
     
     public static String reverse(String s) {
    	 if(s==null||s.isEmpty())
    		 return s;
    	 else
    	 {
    		 StringBuilder str= new StringBuilder(s);
    		
    		 return  str.reverse().toString();
    	 }
    	 
     }
     
     public static void breakword(String s) {
    	 if(s==null||s.isEmpty())
    		 System.out.println(s);
    	 else {
    		 
    		 String str[] =s.split(" ");
    		 for(String ele :str) {
    			 
    			 System.out.print(reverse(ele)+" ");
    			 
    		 }
    		 
    		 
    	 }
    	 
     }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(isUpperCase("SEWDGG"));
		System.out.println(PASSWORD("SEWDGeG"));
		System.out.println(normalize("SEWDG,eG"));
		System.out.println(charatevenIndex('W',"SEWDG,eG"));
		breakword("SEWDG,eG dfags i have a bag i have a white space");
		Optional<String> str=null;
		//str.orElse("");
		
		OptionalInt t = Arrays.stream(new int[]{1,2,3,4}).filter(i->i==4).findFirst();
		
		t.ifPresent(System.out::println);
	}

}
